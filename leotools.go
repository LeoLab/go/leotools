package leotools

import (
	"crypto/rand"
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"
)

/*
	Version: 0.1.0
*/
//Разобрать аргументы командрой строки
//@DEPRECATED -> gitlab.com/leolab/go/AppConf
func ParseArgs() (args map[string]string, cmds []string) {
	args = make(map[string]string)
	cmds = make([]string, 0)
	for i, s := range os.Args {
		if i == 0 {
			//			args["cmd"] = s //Первый параметр - имя запускаемого файла, пропускаем
		} else {
			if strings.HasPrefix(s, "--") { // Длинный ключ, может быть с параметром
				ts := strings.TrimPrefix(s, "--")
				// Может быть с параметром
				if strings.ContainsRune(ts, '=') {
					sa := strings.SplitN(ts, "=", 2)
					args[sa[0]] = sa[1]
				} else {
					args[ts] = ""
				}
			} else if strings.HasPrefix(s, "-") { // Короткий ключ, может быть составным
				ts := strings.TrimPrefix(s, "-")
				tsa := strings.Split(ts, "")
				for _, ss := range tsa {
					args[ss] = ""
				}
			} else { // Типа, команда
				cmds = append(cmds, s)
			}
		}
	}
	return args, cmds

}

/*
	Version: 0.1.0
*/
//Собрать аргументы командной строки
//@DEPRECATED -> gitlab.com/leolab/go/Appconf
func ImplodeArgs(Args map[string]string, Cmds []string) []string {
	ret := make([]string, 0)
	ret = append(ret, Cmds...)
	for k, v := range Args {
		if v == "" {
			if len(k) == 1 {
				ret = append(ret, "-"+k)
			} else {
				ret = append(ret, "--"+k)
			}
		} else {
			ret = append(ret, "--"+k+"=\""+v+"\"")
		}
	}
	return ret
}

/*
	Version: 0.2.1
*/
//Получить уникальную последовательность
func GetCode(length int) string {
	b := make([]byte, length)
	_, err := rand.Read(b)
	if err != nil {
		return ""
	}
	return fmt.Sprintf("%x", b)
}

type SrcRec struct {
	File   string
	Line   int
	Func   string
	GoId   int
	ProcId int
}

//Получить функцию и адрес
func GetSrc(lvl int) (s SrcRec) {
	pc, file, line, ok := runtime.Caller(lvl)
	if !ok {
		return SrcRec{File: "undefined", Line: 0, Func: "undefined()"}
	}
	f := runtime.CallersFrames([]uintptr{pc})
	ff, _ := f.Next()
	s = SrcRec{
		File:   file,
		Line:   line,
		Func:   ff.Function,
		GoId:   GetGoId(),
		ProcId: os.Getpid(),
	}
	return s
}

//Получить идентификатор рутины
func GetGoId() int {
	var buf [64]byte
	n := runtime.Stack(buf[:], false)
	r, e := strconv.ParseInt(strings.Fields(strings.TrimPrefix(string(buf[:n]), "goroutine "))[0], 10, 64)
	if e != nil {
		panic(e)
	}
	return int(r)
}
