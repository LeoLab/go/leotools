package leotools

import "time"

type Duration struct {
	Days    int
	Hours   int
	Minutes int
	Seconds int
}

func GetDuration(src time.Duration) (dst *Duration) {
	dst = &Duration{}
	dst.Days = int(src.Hours()) / 24
	dst.Hours = int(src.Hours()) - dst.Days*24
	dst.Minutes = int(src.Minutes()) - int(dst.Days*24*60) - int(dst.Hours*60)
	dst.Seconds = int(src.Seconds()) - dst.Minutes*60 - dst.Hours*60*60 - dst.Days*24*60*60
	return dst
}
