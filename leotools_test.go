package leotools

import (
	"fmt"
	"testing"
	"time"
)

func TestDuration(t *testing.T) {
	t1 := time.Now()
	t2 := time.Now().Add(time.Hour*74 + time.Minute*14 + time.Second*24)
	d := t2.Sub(t1)
	fmt.Println(GetDuration(d))
}

func TestJoinMap(t *testing.T) {
	m1 := map[string]interface{}{
		"one": map[string]interface{}{
			"sub1": "val1",
			"sub2": "val2",
		},
		"two":  "two-1",
		"tree": "tree-1",
	}
	m2 := map[string]interface{}{
		"one": map[string]interface{}{
			"sub1": "val1-2",
			"sub3": "val3",
		},
		"two":  "two-2",
		"four": "four",
	}

	r := JoinMap(m1, m2)
	fmt.Println("+x", r)
}
